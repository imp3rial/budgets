/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mumba
 */
@Entity
@Table(name = "userGroups", catalog = "assignment", schema = "")
@NamedQueries({
    @NamedQuery(name = "UserGroups.findAll", query = "SELECT u FROM UserGroups u"),
    @NamedQuery(name = "UserGroups.findByIdUsers", query = "SELECT u FROM UserGroups u WHERE u.idUsers = :idUsers"),
    @NamedQuery(name = "UserGroups.findByDescription", query = "SELECT u FROM UserGroups u WHERE u.description = :description"),
    @NamedQuery(name = "UserGroups.findByAccount", query = "SELECT u FROM UserGroups u WHERE u.account = :account"),
    @NamedQuery(name = "UserGroups.findByBudget", query = "SELECT u FROM UserGroups u WHERE u.budget = :budget"),
    @NamedQuery(name = "UserGroups.findByCreatedAt", query = "SELECT u FROM UserGroups u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "UserGroups.findByUpdatedAt", query = "SELECT u FROM UserGroups u WHERE u.updatedAt = :updatedAt")})
public class UserGroups implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUsers")
    private Integer idUsers;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "account")
    private int account;
    @Basic(optional = false)
    @Column(name = "budget")
    private int budget;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public UserGroups() {
    }

    public UserGroups(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public UserGroups(Integer idUsers, String description, int account, int budget) {
        this.idUsers = idUsers;
        this.description = description;
        this.account = account;
        this.budget = budget;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        Integer oldIdUsers = this.idUsers;
        this.idUsers = idUsers;
        changeSupport.firePropertyChange("idUsers", oldIdUsers, idUsers);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        String oldDescription = this.description;
        this.description = description;
        changeSupport.firePropertyChange("description", oldDescription, description);
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        int oldAccount = this.account;
        this.account = account;
        changeSupport.firePropertyChange("account", oldAccount, account);
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        int oldBudget = this.budget;
        this.budget = budget;
        changeSupport.firePropertyChange("budget", oldBudget, budget);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        Date oldCreatedAt = this.createdAt;
        this.createdAt = createdAt;
        changeSupport.firePropertyChange("createdAt", oldCreatedAt, createdAt);
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        Date oldUpdatedAt = this.updatedAt;
        this.updatedAt = updatedAt;
        changeSupport.firePropertyChange("updatedAt", oldUpdatedAt, updatedAt);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsers != null ? idUsers.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserGroups)) {
            return false;
        }
        UserGroups other = (UserGroups) object;
        if ((this.idUsers == null && other.idUsers != null) || (this.idUsers != null && !this.idUsers.equals(other.idUsers))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "views.UserGroups[ idUsers=" + idUsers + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
