/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author mumba
 */
@Entity
@Table(name = "expenditure", catalog = "assignment", schema = "")
@NamedQueries({
    @NamedQuery(name = "Expenditure.findAll", query = "SELECT e FROM Expenditure e"),
    @NamedQuery(name = "Expenditure.findByIdExpenditure", query = "SELECT e FROM Expenditure e WHERE e.idExpenditure = :idExpenditure"),
    @NamedQuery(name = "Expenditure.findByPurpose", query = "SELECT e FROM Expenditure e WHERE e.purpose = :purpose"),
    @NamedQuery(name = "Expenditure.findByAmount", query = "SELECT e FROM Expenditure e WHERE e.amount = :amount"),
    @NamedQuery(name = "Expenditure.findByAccount", query = "SELECT e FROM Expenditure e WHERE e.account = :account"),
    @NamedQuery(name = "Expenditure.findByGroup", query = "SELECT e FROM Expenditure e WHERE e.group = :group"),
    @NamedQuery(name = "Expenditure.findByUser", query = "SELECT e FROM Expenditure e WHERE e.user = :user")})
public class Expenditure implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idExpenditure")
    private Integer idExpenditure;
    @Basic(optional = false)
    @Column(name = "purpose")
    private String purpose;
    @Basic(optional = false)
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @Column(name = "account")
    private int account;
    @Column(name = "group")
    private String group;
    @Column(name = "user")
    private Integer user;

    public Expenditure() {
    }

    public Expenditure(Integer idExpenditure) {
        this.idExpenditure = idExpenditure;
    }

    public Expenditure(Integer idExpenditure, String purpose, double amount, int account) {
        this.idExpenditure = idExpenditure;
        this.purpose = purpose;
        this.amount = amount;
        this.account = account;
    }

    public Integer getIdExpenditure() {
        return idExpenditure;
    }

    public void setIdExpenditure(Integer idExpenditure) {
        Integer oldIdExpenditure = this.idExpenditure;
        this.idExpenditure = idExpenditure;
        changeSupport.firePropertyChange("idExpenditure", oldIdExpenditure, idExpenditure);
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        String oldPurpose = this.purpose;
        this.purpose = purpose;
        changeSupport.firePropertyChange("purpose", oldPurpose, purpose);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        double oldAmount = this.amount;
        this.amount = amount;
        changeSupport.firePropertyChange("amount", oldAmount, amount);
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        int oldAccount = this.account;
        this.account = account;
        changeSupport.firePropertyChange("account", oldAccount, account);
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        String oldGroup = this.group;
        this.group = group;
        changeSupport.firePropertyChange("group", oldGroup, group);
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        Integer oldUser = this.user;
        this.user = user;
        changeSupport.firePropertyChange("user", oldUser, user);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExpenditure != null ? idExpenditure.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expenditure)) {
            return false;
        }
        Expenditure other = (Expenditure) object;
        if ((this.idExpenditure == null && other.idExpenditure != null) || (this.idExpenditure != null && !this.idExpenditure.equals(other.idExpenditure))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "views.Expenditure[ idExpenditure=" + idExpenditure + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
