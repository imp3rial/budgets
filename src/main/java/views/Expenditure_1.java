/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mumba
 */
@Entity
@Table(name = "Expenditure", catalog = "assignment", schema = "")
@NamedQueries({
    @NamedQuery(name = "Expenditure_1.findAll", query = "SELECT e FROM Expenditure_1 e"),
    @NamedQuery(name = "Expenditure_1.findByIdExpenditure", query = "SELECT e FROM Expenditure_1 e WHERE e.idExpenditure = :idExpenditure"),
    @NamedQuery(name = "Expenditure_1.findByPurpose", query = "SELECT e FROM Expenditure_1 e WHERE e.purpose = :purpose"),
    @NamedQuery(name = "Expenditure_1.findByAmount", query = "SELECT e FROM Expenditure_1 e WHERE e.amount = :amount"),
    @NamedQuery(name = "Expenditure_1.findByAccount", query = "SELECT e FROM Expenditure_1 e WHERE e.account = :account"),
    @NamedQuery(name = "Expenditure_1.findByUserGroup", query = "SELECT e FROM Expenditure_1 e WHERE e.userGroup = :userGroup"),
    @NamedQuery(name = "Expenditure_1.findByUser", query = "SELECT e FROM Expenditure_1 e WHERE e.user = :user"),
    @NamedQuery(name = "Expenditure_1.findByCreatedAt", query = "SELECT e FROM Expenditure_1 e WHERE e.createdAt = :createdAt"),
    @NamedQuery(name = "Expenditure_1.findByUpdatedAt", query = "SELECT e FROM Expenditure_1 e WHERE e.updatedAt = :updatedAt")})
public class Expenditure_1 implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idExpenditure")
    private Integer idExpenditure;
    @Basic(optional = false)
    @Column(name = "purpose")
    private String purpose;
    @Basic(optional = false)
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @Column(name = "account")
    private int account;
    @Column(name = "userGroup")
    private String userGroup;
    @Column(name = "user")
    private Integer user;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public Expenditure_1() {
    }

    public Expenditure_1(Integer idExpenditure) {
        this.idExpenditure = idExpenditure;
    }

    public Expenditure_1(Integer idExpenditure, String purpose, double amount, int account) {
        this.idExpenditure = idExpenditure;
        this.purpose = purpose;
        this.amount = amount;
        this.account = account;
    }

    public Integer getIdExpenditure() {
        return idExpenditure;
    }

    public void setIdExpenditure(Integer idExpenditure) {
        Integer oldIdExpenditure = this.idExpenditure;
        this.idExpenditure = idExpenditure;
        changeSupport.firePropertyChange("idExpenditure", oldIdExpenditure, idExpenditure);
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        String oldPurpose = this.purpose;
        this.purpose = purpose;
        changeSupport.firePropertyChange("purpose", oldPurpose, purpose);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        double oldAmount = this.amount;
        this.amount = amount;
        changeSupport.firePropertyChange("amount", oldAmount, amount);
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        int oldAccount = this.account;
        this.account = account;
        changeSupport.firePropertyChange("account", oldAccount, account);
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        String oldUserGroup = this.userGroup;
        this.userGroup = userGroup;
        changeSupport.firePropertyChange("userGroup", oldUserGroup, userGroup);
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        Integer oldUser = this.user;
        this.user = user;
        changeSupport.firePropertyChange("user", oldUser, user);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        Date oldCreatedAt = this.createdAt;
        this.createdAt = createdAt;
        changeSupport.firePropertyChange("createdAt", oldCreatedAt, createdAt);
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        Date oldUpdatedAt = this.updatedAt;
        this.updatedAt = updatedAt;
        changeSupport.firePropertyChange("updatedAt", oldUpdatedAt, updatedAt);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExpenditure != null ? idExpenditure.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expenditure_1)) {
            return false;
        }
        Expenditure_1 other = (Expenditure_1) object;
        if ((this.idExpenditure == null && other.idExpenditure != null) || (this.idExpenditure != null && !this.idExpenditure.equals(other.idExpenditure))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "views.Expenditure_1[ idExpenditure=" + idExpenditure + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
