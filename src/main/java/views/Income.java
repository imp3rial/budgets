/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author mumba
 */
@Entity
@Table(name = "Income", catalog = "assignment", schema = "")
@NamedQueries({
    @NamedQuery(name = "Income.findAll", query = "SELECT i FROM Income i"),
    @NamedQuery(name = "Income.findByIdIncome", query = "SELECT i FROM Income i WHERE i.idIncome = :idIncome"),
    @NamedQuery(name = "Income.findBySource", query = "SELECT i FROM Income i WHERE i.source = :source"),
    @NamedQuery(name = "Income.findByAmount", query = "SELECT i FROM Income i WHERE i.amount = :amount"),
    @NamedQuery(name = "Income.findByAccount", query = "SELECT i FROM Income i WHERE i.account = :account"),
    @NamedQuery(name = "Income.findByCreatedAt", query = "SELECT i FROM Income i WHERE i.createdAt = :createdAt"),
    @NamedQuery(name = "Income.findByUpdatedAt", query = "SELECT i FROM Income i WHERE i.updatedAt = :updatedAt")})
public class Income implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idIncome")
    private Integer idIncome;
    @Basic(optional = false)
    @Column(name = "source")
    private String source;
    @Basic(optional = false)
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @Column(name = "account")
    private int account;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public Income() {
    }

    public Income(Integer idIncome) {
        this.idIncome = idIncome;
    }

    public Income(Integer idIncome, String source, double amount, int account) {
        this.idIncome = idIncome;
        this.source = source;
        this.amount = amount;
        this.account = account;
    }

    public Integer getIdIncome() {
        return idIncome;
    }

    public void setIdIncome(Integer idIncome) {
        Integer oldIdIncome = this.idIncome;
        this.idIncome = idIncome;
        changeSupport.firePropertyChange("idIncome", oldIdIncome, idIncome);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        String oldSource = this.source;
        this.source = source;
        changeSupport.firePropertyChange("source", oldSource, source);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        double oldAmount = this.amount;
        this.amount = amount;
        changeSupport.firePropertyChange("amount", oldAmount, amount);
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        int oldAccount = this.account;
        this.account = account;
        changeSupport.firePropertyChange("account", oldAccount, account);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        Date oldCreatedAt = this.createdAt;
        this.createdAt = createdAt;
        changeSupport.firePropertyChange("createdAt", oldCreatedAt, createdAt);
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        Date oldUpdatedAt = this.updatedAt;
        this.updatedAt = updatedAt;
        changeSupport.firePropertyChange("updatedAt", oldUpdatedAt, updatedAt);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIncome != null ? idIncome.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Income)) {
            return false;
        }
        Income other = (Income) object;
        if ((this.idIncome == null && other.idIncome != null) || (this.idIncome != null && !this.idIncome.equals(other.idIncome))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "views.Income[ idIncome=" + idIncome + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
