-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 07, 2017 at 12:44 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 5.6.29-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment`
--

-- --------------------------------------------------------

--
-- Table structure for table `Accounts`
--

CREATE TABLE `Accounts` (
  `accountNo` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Accounts`
--

INSERT INTO `Accounts` (`accountNo`, `description`, `type`, `created_at`, `updated_at`) VALUES
(30201, 'Tuions', 'Item 2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accountTypes`
--

CREATE TABLE `accountTypes` (
  `idAccountType` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accountTypes`
--

INSERT INTO `accountTypes` (`idAccountType`, `description`) VALUES
(1, 'home'),
(2, 'shared'),
(3, 'descretionary');

-- --------------------------------------------------------

--
-- Table structure for table `Budgets`
--

CREATE TABLE `Budgets` (
  `idBudget` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Budgets`
--

INSERT INTO `Budgets` (`idBudget`, `description`, `total`, `created_at`, `updated_at`) VALUES
(1, 'Family budget', 300.00, NULL, NULL),
(2, 'Personal budget', 800.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Expenditure`
--

CREATE TABLE `Expenditure` (
  `idExpenditure` int(10) UNSIGNED NOT NULL,
  `purpose` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `account` int(11) NOT NULL,
  `userGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Expenditure`
--

INSERT INTO `Expenditure` (`idExpenditure`, `purpose`, `amount`, `account`, `userGroup`, `user`, `created_at`, `updated_at`) VALUES
(1, 'Laptop', 4000.00, 0, NULL, NULL, NULL, NULL),
(2, 'Room', 1000.00, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Income`
--

CREATE TABLE `Income` (
  `idIncome` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `account` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Income`
--

INSERT INTO `Income` (`idIncome`, `source`, `amount`, `account`, `created_at`, `updated_at`) VALUES
(1, 'Donation', 700.00, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_05_004509_create_expenditure_table', 2),
(4, '2017_01_05_020914_create_expenditure_table', 3),
(7, '2017_01_05_021219_create_expenditure_table', 4),
(8, '2017_01_05_170621_create_accounts_table', 5),
(9, '2017_01_06_063151_create_userTypes', 6),
(10, '2017_01_06_063159_create_accountTypes', 6),
(11, '2017_01_06_063425_create_income_table', 6),
(12, '2017_01_06_063636_create_usergroups', 6),
(13, '2017_01_06_063656_create_budgets_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userGroups`
--

CREATE TABLE `userGroups` (
  `idUsers` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userGroups`
--

INSERT INTO `userGroups` (`idUsers`, `description`, `account`, `budget`, `created_at`, `updated_at`) VALUES
(1, 'Family', 30201, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUsers` int(10) UNSIGNED NOT NULL,
  `fName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idAccounts` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUsers`, `fName`, `lName`, `email`, `password`, `userGroup`, `userType`, `idAccounts`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Josephine', 'Darakjy', 'calbares@gmail.com', 'admin', 'class', 'admin', 910920, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(2, 'Josephine', 'Darakjy', 'gruta@cox.net', 'admin', 'family', 'admin', 49754, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(3, 'Josephine', 'Darakjy', 'amaclead@gmail.com', 'admin', 'workers', 'other', 9847848, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(4, 'Josephine', 'Darakjy', 'minna_amigon@yahoo.com', 'admin', 'gurads', 'admin', 9848984, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(5, 'Josephine', 'Darakjy', 'josephine_darakjy@darakjy.org', 'admin', 'family', 'other', 9374, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(6, 'Art', 'Venere', 'art@venere.org', 'admin', 'patients', 'other', 98760, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(7, 'Lenna', 'Paprocki', 'lpaprocki@hotmail.com', 'admin', 'family', 'other', 46757, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(8, 'Simona', 'Morasca', 'simona@morasca.com', 'admin', 'family', 'admin', 9789, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(9, 'Mitsue', 'Tollner', 'mitsue_tollner@yahoo.com', 'admin', 'family', 'other', 910920, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45'),
(10, 'Leota', 'Dilliard', 'leota@hotmail.com', 'admin', 'family', 'other', 910920, NULL, '2017-01-06 21:43:45', '2017-01-06 21:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `UserTypes`
--

CREATE TABLE `UserTypes` (
  `idUsers` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `UserTypes`
--

INSERT INTO `UserTypes` (`idUsers`, `description`) VALUES
(1, 'Other'),
(2, 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Accounts`
--
ALTER TABLE `Accounts`
  ADD PRIMARY KEY (`accountNo`);

--
-- Indexes for table `accountTypes`
--
ALTER TABLE `accountTypes`
  ADD PRIMARY KEY (`idAccountType`);

--
-- Indexes for table `Budgets`
--
ALTER TABLE `Budgets`
  ADD PRIMARY KEY (`idBudget`);

--
-- Indexes for table `Expenditure`
--
ALTER TABLE `Expenditure`
  ADD PRIMARY KEY (`idExpenditure`);

--
-- Indexes for table `Income`
--
ALTER TABLE `Income`
  ADD PRIMARY KEY (`idIncome`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `userGroups`
--
ALTER TABLE `userGroups`
  ADD PRIMARY KEY (`idUsers`),
  ADD UNIQUE KEY `idUsers` (`idUsers`),
  ADD KEY `fk_id` (`account`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `UserTypes`
--
ALTER TABLE `UserTypes`
  ADD PRIMARY KEY (`idUsers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountTypes`
--
ALTER TABLE `accountTypes`
  MODIFY `idAccountType` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Budgets`
--
ALTER TABLE `Budgets`
  MODIFY `idBudget` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Expenditure`
--
ALTER TABLE `Expenditure`
  MODIFY `idExpenditure` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Income`
--
ALTER TABLE `Income`
  MODIFY `idIncome` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `userGroups`
--
ALTER TABLE `userGroups`
  MODIFY `idUsers` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `UserTypes`
--
ALTER TABLE `UserTypes`
  MODIFY `idUsers` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `userGroups`
--
ALTER TABLE `userGroups`
  ADD CONSTRAINT `fk_id` FOREIGN KEY (`account`) REFERENCES `Accounts` (`accountNo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
